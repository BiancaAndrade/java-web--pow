package Modelo;
/**classe Imagem
 * @author Bianca Andrade
 *version 1.8
 */
public class Imagem {
	private int id;
	private String imagem;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getImagem() {
		return imagem;
	}
	public void setImagem(String imagem) {
		this.imagem = imagem;
	}
}

package Modelo;
/**classe Video
 * @author Bianca Andrade
 *version 1.8
 */
public class Video {
	private String video;
	private int id;
	public String getVideo() {
		return video;
	}
	public void setVideo(String video) {
		this.video = video;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
}

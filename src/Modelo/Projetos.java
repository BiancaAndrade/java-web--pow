package Modelo;
/**classe Projetos
 * @author Bianca Andrade
 *version 1.8
 */
public class Projetos {
	private String nome;
	private int id;
	private String imagem;
public Projetos() {
		// TODO Auto-generated constructor stub
	}	
	public String getNome() {
		return nome;
	}
	public String getImagem() {
		return imagem;
	}

	public void setImagem(String imagem) {
		this.imagem = imagem;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
}

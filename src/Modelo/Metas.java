package Modelo;
/**classe Metas
 * @author Bianca Andrade
 *version 1.8
 */
public class Metas {
	private String nome;
	private String obs;
	private int id;
	public String getNome() {	
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getObs() {
		return obs;
	}
	public void setObs(String obs) {
		this.obs = obs;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @param id
	 * @param nome
	 * @param obs
	 */
	public Metas(int id,String nome, String obs) {
		this.nome = nome;
		this.obs = obs;
		this.id = id;
	}
	
	
}

package Controle;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import Modelo.Imagem;
/**classe ControleImagem
 * @author Bianca Andrade
 *version 1.8
 */
public class ControleImagem {
	/**M�todo para inserir registros da tabela imagem
	 * @param img
	 * @return boolean
	 */
	public boolean inserir(Imagem img){	
		boolean resultado = false;
		Connection con = new Conexao().abrirConexao();
		try {
			PreparedStatement ps = con.prepareStatement("INSERT INTO imagem(imagem) VALUES(?);");
			ps.setString(1, img.getImagem());
			if(!ps.execute()) {
				resultado = true;
			}
			new Conexao().fecharConexao(con);
		}catch(SQLException e) {
			System.out.println(e.getMessage());
		}
		return resultado;
	}
	/**M�todo para selecionar registros da tabela imagem
	 * @return registros
	 */
	public ArrayList<Imagem> consultarTodos(){
		ArrayList<Imagem> lista = null;
		try {
			Connection con = new Conexao().abrirConexao();
			PreparedStatement ps = con.prepareStatement("SELECT * FROM imagem;");
			ResultSet rs = ps.executeQuery();
			if(rs != null) {
				lista = new ArrayList<Imagem>();
				while(rs.next()) {
					Imagem img = new Imagem();
					img.setId(rs.getInt("id"));
					img.setImagem(rs.getString("imagem"));
					lista.add(img);
				}
			}
			new Conexao().fecharConexao(con);
		}catch(SQLException e) {
			System.out.println("Erro no servidor: " + e.getMessage());
		}
		return lista;
	}
}
package Controle;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import Modelo.Metas;
/**classe ControleMetas
 * @author Bianca Andrade
 *version 1.8
 */
public class ControleMetas {
	/**insere registro da tabela metas
	 * @param me
	 * @return boolean
	 */
	public boolean inserir(Metas me){
		
		boolean resultado = false;
		Connection con = new Conexao().abrirConexao();
		try {
			PreparedStatement ps = con.prepareStatement("INSERT INTO metas(nome,obs) VALUES(?,?);");
			ps.setString(1, me.getNome());
			ps.setString(2, me.getObs());
			if(!ps.execute()) {
				resultado = true;
			}
			new Conexao().fecharConexao(con);
		}catch(SQLException e) {
			System.out.println(e.getMessage());
		}
		return resultado;
	}
	/**seleciona registros da tabela metas
	 * @return registros
	 */
	public ArrayList<Metas> consultarTodos(){
		ArrayList<Metas> lista = null;
		try {
			Connection con = new Conexao().abrirConexao();
			PreparedStatement ps = con.prepareStatement("SELECT * FROM metas;");
			ResultSet rs = ps.executeQuery();
			if(rs != null) {
				lista = new ArrayList<Metas>();
				while(rs.next()) {
					Metas me = new Metas(0, null, null);
					me.setId(rs.getInt("id"));
					me.setNome(rs.getString("nome"));
					me.setObs(rs.getString("obs"));
					lista.add(me);
				}
			}
			new Conexao().fecharConexao(con);
		}catch(SQLException e) {
			System.out.println("Erro no servidor: " + e.getMessage());
		}
		return lista;
	}
	/**atualiza registros da tabela metas
	 * @param me
	 * @return boolean
	 */
	public boolean atualizar(Metas me){
		
		boolean resultado = false;
		try {
			Connection con = new Conexao().abrirConexao();
			System.out.println(me.getId());
			System.out.println(me.getNome());
			System.out.println(me.getObs());
			PreparedStatement ps = con.prepareStatement("UPDATE metas SET nome=?, obs=? WHERE id=?");
			ps.setString(1, me.getNome());
			ps.setString(2, me.getObs());
			ps.setInt(3, me.getId());
			
			if(!ps.execute()){
				resultado = true;
			}
			new Conexao().fecharConexao(con);
		}catch(SQLException e) {
			System.out.println("Erro ao editar: " + e.getMessage());
		}
	return resultado;
}
	/**deleta registros da tabela metas
	 * @param id
	 * @return boolean
	 */
	public boolean deletar(int id){
		
		boolean resultado= false;
		Connection con = new Conexao().abrirConexao();
		try {
		PreparedStatement ps = con.prepareStatement("DELETE FROM metas WHERE id=?;");
		ps.setInt(1, id);
		if(!ps.execute()){
			resultado = true;
			new Conexao().fecharConexao(con);
		}else {
			throw new SQLException("Erro ao inserir as informações.");
		}
		}catch(SQLException e) {
			System.out.println(e.getMessage());
		}				
		return resultado;
	}
	/**consulta registros da tabela metas
	 * @param id
	 * @return registros
	 */
	public Metas consultaUm(int id) {
		
		Metas me = null;
		try {
			Connection con = new Conexao().abrirConexao();
			PreparedStatement ps = con.prepareStatement("SELECT * FROM metas WHERE id=?");
			ps.setInt(1, id);
			ResultSet rs = ps.executeQuery();
			if(rs.next()) {
				me = new Metas(id, null, null);
				me.setId(rs.getInt("id"));
				me.setNome(rs.getString("nome"));
				me.setObs(rs.getString("obs"));
				new Conexao().fecharConexao(con);
			}
	}catch(SQLException e) {
		System.out.println("Erro no servidor: " + e.getMessage());
	}
		
		return me;
	}
}

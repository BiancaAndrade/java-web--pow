package Controle;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import Modelo.Projetos;
import Controle.Conexao;
/**classe ControleUsuario
 * @author Bianca Andrade
 *version 1.8
 */
public class ControleProjetos {
	
	/**insere registros na tabela projetos
	 * @param pro
	 * @return boolean
	 */
	public boolean inserir(Projetos pro){
		
		boolean resultado = false;
		Connection con = new Conexao().abrirConexao();
		try {
			PreparedStatement ps = con.prepareStatement("INSERT INTO projetos(nome,imagem) VALUES(?,?);");
			ps.setString(1, pro.getNome());
			ps.setString(2, pro.getImagem());
			if(!ps.execute()) {
				resultado = true;
			}
			new Conexao().fecharConexao(con);
		}catch(SQLException e) {
			System.out.println(e.getMessage());
		}
		return resultado;
	}
	/**seleciona registros da tabela projetos
	 * @return registros
	 */
	public ArrayList<Projetos> consultarTodos(){
		
		ArrayList<Projetos> lista = null;
		try {
			Connection con = new Conexao().abrirConexao();
			PreparedStatement ps = con.prepareStatement("SELECT * FROM projetos;");
			ResultSet rs = ps.executeQuery();
			if(rs != null) {
				lista = new ArrayList<Projetos>();
				while(rs.next()) {
					Projetos pro = new Projetos();
					pro.setId(rs.getInt("id"));
					pro.setNome(rs.getString("nome"));
					pro.setImagem(rs.getString("imagem"));
					lista.add(pro);
				}
			}
			new Conexao().fecharConexao(con);
		}catch(SQLException e) {
			System.out.println("Erro no servidor: " + e.getMessage());
		}
		return lista;
	}
	/**atualiza registros da tabela projetos
	 * @param pro
	 * @return boolean
	 */
	public boolean atualizar(Projetos pro){
		
		boolean resultado = false;
		try {
			Connection con = new Conexao().abrirConexao();
			PreparedStatement ps = con.prepareStatement("UPDATE projetos SET nome=? WHERE id=?");
			ps.setString(1, pro.getNome());
			ps.setInt(2, pro.getId());
			if(!ps.execute()){
				resultado = true;
			}
			new Conexao().fecharConexao(con);
		}catch(SQLException e) {
			System.out.println("Erro ao editar: " + e.getMessage());
		}
	return resultado;
}
	/** deleta registros da tabela projetos
	 * @param id
	 * @return boolean
	 */
	public boolean deletar(int id){
		
		boolean resultado= false;
		Connection con = new Conexao().abrirConexao();
		try {
		PreparedStatement ps = con.prepareStatement("DELETE FROM projetos WHERE id=?;");
		ps.setInt(1, id);
		if(!ps.execute()){
			resultado = true;
			new Conexao().fecharConexao(con);
		}else {
			throw new SQLException("Erro ao inserir as informações.");
		}
		}catch(SQLException e) {
			System.out.println(e.getMessage());
		}				
		return resultado;
	}
	/** consulta registros da tabela projetos
	 * @param id
	 * @return registros
	 */
	public Projetos consultaUm(int id) {
		
		Projetos pro = null;
		try {
			Connection con = new Conexao().abrirConexao();
			PreparedStatement ps = con.prepareStatement("SELECT * FROM projetos WHERE id=?");
			ps.setInt(1, id);
			ResultSet rs = ps.executeQuery();
			if(rs.next()) {
				pro = new Projetos();
				pro.setId(rs.getInt("id"));
				pro.setNome(rs.getString("nome"));
				pro.setImagem(rs.getString("imagem"));
				new Conexao().fecharConexao(con);
			}
	}catch(SQLException e) {
		System.out.println("Erro no servidor: " + e.getMessage());
	}
		
		return pro;
	}

	
}

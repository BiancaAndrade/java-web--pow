package Controle;
import java.sql.Connection; //abrir e fechar a conexao
import java.sql.SQLException;
//import com.mysql.jdbc.Driver;
import java.sql.DriverManager;
/**classe Conex�o
 * @author Bianca Andrade
 *version 1.8
 */
public final class Conexao {
	/**M�todo para conectar com Banco de dados
	 * @return
	 */
	public Connection abrirConexao() {
		
		Connection connect = null ;
		try {
			Class.forName("com.mysql.jdbc.Driver");
			String banco = "pow";
			String servidor = "jdbc:mysql://localhost/" + banco;
			String user = "root";
			String pwd ="";
			connect = DriverManager.getConnection(servidor, user, pwd);
		}catch(SQLException e ) {
			e.getMessage();
		}catch(Exception e) {
			e.getMessage();
		}
		return connect;	
	}
	/**M�todo para fechar conex�o com o banco de dados
	 * @param con
	 */
	public void fecharConexao(Connection con){
		
		try {
			con.close();
		}catch(Exception e) {
			e.getMessage();
		}
	}
}

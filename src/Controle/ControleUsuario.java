package Controle;
import Controle.Crypt;
import java.sql.Connection;
import java.sql.SQLException;
import Modelo.Usuario;
import Controle.Conexao;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
/**classe ControleUsuario
 * @author Bianca Andrade
 *version 1.8
 */
public class ControleUsuario {
	
	/**seleciona com o uso de dois registros de tabela
	 * @param user
	 * @return registros
	 * @throws SQLException
	 */
	public Usuario logar(Usuario user) throws SQLException{
		Connection con = new Conexao().abrirConexao();
		PreparedStatement ps = con.prepareStatement("SELECT * FROM usuario WHERE email=? and senha=?");
		ps.setString(1, user.getEmail());
		ps.setString(2, user.getSenha());
		ResultSet rs = ps.executeQuery();
		if(rs.next() && rs != null){
			user.setId(rs.getInt("id"));
			user.setNome(rs.getString("nome"));
			user.setEmail(rs.getString("email"));
			user.setSenha(rs.getString("senha"));
		}else{
			user=null;
		}
		new Conexao().fecharConexao(con);	
		return user;
	}
	/**insere registros na tabela usuario
	 * @param user
	 * @return boolean
	 */
	public boolean inserir(Usuario user) {
		
		boolean resultado = false;
		try {
			Crypt c = new Crypt();
			String key = "kjtgihkjmtgfhtfntgdghfyk"+
			"khkjhkjyfytdgkijlohkjhk"+
			"tgbhokjhkgjugukhfujgiug"+
			"hfdcytfyuhjgfiugthujgjh";
			String senha= user.getSenha();
			byte[] re= c.encrypt(senha, key);
			Connection con = new Conexao().abrirConexao();
			PreparedStatement ps = con.prepareStatement("INSERT INTO usuario(nome,email,senha) VALUES (?,?,?)");
			ps.setString(1, user.getNome());
			ps.setString(2, user.getEmail());
			ps.setBytes(3, re);
			if(!ps.execute()) {
				resultado = true;
			}
			new Conexao().fecharConexao(con);
		}catch(SQLException e) {
			System.out.println("Erro ao inserir registro." + e.getMessage());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return resultado;
	}

	
}

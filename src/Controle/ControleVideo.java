package Controle;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import Modelo.Video;
/**classe ControleVideo
 * @author Bianca Andrade
 *version 1.8
 */
public class ControleVideo {
	
/**M�todo para inserir registros na tabela video
 * @param vd
 * @return boolean
 */
public boolean inserir(Video vd){
	 
		boolean resultado = false;
		Connection con = new Conexao().abrirConexao();
		try {
			PreparedStatement ps = con.prepareStatement("INSERT INTO video(video) VALUES(?);");
			ps.setString(1, vd.getVideo());
			if(!ps.execute()) {
				resultado = true;
			}
			new Conexao().fecharConexao(con);
		}catch(SQLException e) {
			System.out.println(e.getMessage());
		}
		return resultado;
	}
 
	
	/**selecionar registro da tabela video
	 * @return registros
	 */
	public ArrayList<Video> consultarTodos(){
		
		ArrayList<Video> lista = null;
		try {
			Connection con = new Conexao().abrirConexao();
			PreparedStatement ps = con.prepareStatement("SELECT * FROM video;");
			ResultSet rs = ps.executeQuery();
			if(rs != null) {
				lista = new ArrayList<Video>();
				while(rs.next()) {
					Video vd = new Video();
					vd.setId(rs.getInt("id"));
					vd.setVideo(rs.getString("video"));
					lista.add(vd);
				}
			}
			new Conexao().fecharConexao(con);
		}catch(SQLException e) {
			System.out.println("Erro no servidor: " + e.getMessage());
		}
		return lista;
	}
}

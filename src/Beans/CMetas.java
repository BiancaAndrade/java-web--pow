package Beans;
import java.util.ArrayList;
import javax.faces.bean.ManagedBean;
import Controle.ControleMetas;
import Modelo.Metas;
/**classe CMetas
 * @author Bianca Andrade
 *version 1.8
 */
@ManagedBean(name="CMetas")
public class CMetas {
	private ArrayList<Metas> lista = new ControleMetas().consultarTodos();
	private String nome;
	private String obs;
	private int id;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public ArrayList<Metas> getLista() {
		return lista;
	}
	public void setLista(ArrayList<Metas> lista) {
		this.lista = lista;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getObs() {
		return obs;
	}
	public void setObs(String obs) {
		this.obs = obs;
	}
	
	/**recebe e redireciona registro para o metodo de inserir 
	 * @return boolean
	 */
	public boolean adc() {
		boolean result= false;
		Metas me= new Metas(id, nome, obs);
		ControleMetas co =new ControleMetas();
		try {
			me.setNome(nome);
			me.setObs(obs);
			co.inserir(me);
			result=true;
		}catch(Exception e) {
			e.getMessage();
		}
		return result;
	}
	/**recebe e redireciona registro para o metodo de deletar
	 * @param id
	 * @return String
	 */
	public String deletar(int id) {
		new ControleMetas().deletar(id);
		return "metas";
	}
	/**recebe e redireciona registro para o metodo de consultar
	 * @param id
	 */
	public void carregarId(int id) {
		Metas me = new ControleMetas().consultaUm(id);
		this.setId(me.getId());
		this.setNome(me.getNome());
		this.setObs(me.getObs());
	}
	/**recebe e redireciona registro para o metodo de editar
	 * @return String
	 */
	public String editar() {
		Metas me= new Metas(this.getId(),this.getNome(),this.getObs());
		
		if(new ControleMetas().atualizar(me)) {
			System.out.println("Deu certo");
			return "metas";	
		}
		return "atuMetas";
	}

}

package Beans;
import java.io.File;
import java.io.IOException;
import javax.faces.bean.ManagedBean;
import javax.servlet.http.Part;
import Controle.ControleImagem;
import Modelo.Imagem;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
/**classe CImagem
 * @author Bianca Andrade
 *version 1.8
 */
@ManagedBean(name="CImagem")
public class CImagem {
	
	private ArrayList<Imagem> lista = new ControleImagem().consultarTodos();
	private int id;
	private Part imagem;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Part getImagem() {
		return imagem;
	}
	public void setImagem(Part imagem) {
		this.imagem = imagem;
	}
	public ArrayList<Imagem> getLista(){
		return lista;
	}
	/**recebe e redireciona registro para o metodo de inserir
	 * @return String
	 * @throws IOException
	 */
	public String pegarImagem() throws IOException{
		
		ControleImagem co = new ControleImagem();
		Imagem imgm = new Imagem();
		String pasta = "C:\\Users\\Bianca & Biatriz\\Desktop\\MeuProjeto\\WebContent\\imgUser\\";
		InputStream img = imagem.getInputStream();
		String nome = imagem.getSubmittedFileName();
		Files.copy(img, new File(pasta,nome).toPath(), StandardCopyOption.REPLACE_EXISTING);
		String diretImg = "imgUser/"+""+nome;
		imgm.setImagem(diretImg);
		co.inserir(imgm);
		return "imagem";
	}
	
}

package Beans;
import java.io.File;
import java.io.IOException;
import javax.faces.bean.ManagedBean;
import javax.servlet.http.Part;
import Controle.ControleVideo;
import Modelo.Video;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
/**classe CVideo
 * @author Bianca Andrade
 *version 1.8
 */
@ManagedBean(name="CVideo")
public class CVideo {
	private ArrayList<Video> lista = new ControleVideo().consultarTodos();
	private Part video;
	private int id;
	public Part getVideo() {
		return video;
	}
	public void setVideo(Part video) {
		this.video = video;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public ArrayList<Video> getLista(){
		return lista;
	}
	/**recebe e redireciona registro para o metodo de inserir
	 * @return String
	 * @throws IOException
	 */
	public String pegarVideo() throws IOException{
		ControleVideo co = new ControleVideo();
		Video vd = new Video();
		String pasta = "C:\\Users\\Bianca & Biatriz\\Desktop\\MeuProjeto\\WebContent\\videoUser\\";
		InputStream vid = video.getInputStream();
		String nome = video.getSubmittedFileName();
		Files.copy(vid, new File(pasta,nome).toPath(), StandardCopyOption.REPLACE_EXISTING);
		String diretVideo = "videoUser/"+""+nome;
		vd.setVideo(diretVideo);
		co.inserir(vd);
		return "video";
	}
}

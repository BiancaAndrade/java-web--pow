package Beans;

import javax.faces.bean.ManagedBean;

import Controle.ControleUsuario;
import Modelo.Usuario;
/**classe Cadastro
 * @author Bianca Andrade
 *version 1.8
 */
@ManagedBean(name="Cadastroo")
public class Cadastro {
	private String nome;
	private String email;
	private String senha;
	private int id;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getSenha() {
		return senha;
	}
	public void setSenha(String senha) {
		this.senha = senha;
	}
	/**recebe e redireciona dados para o metodo de inserir
	 * @return boolean
	 */
	public boolean adc() {
		boolean result= false;
		Usuario user= new Usuario();
		ControleUsuario co =new ControleUsuario();
		try {
			user.setNome(nome);
			user.setSenha(senha);
			user.setEmail(email);
			co.inserir(user);
			result=true;
		}catch(Exception e) {
			e.getMessage();
		}
		return result;
	}

}

package Beans;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import javax.faces.bean.ManagedBean;
import Controle.ControleProjetos;
import Modelo.Projetos;
import javax.servlet.http.Part;
/**classe CProjetos
 * @author Bianca Andrade
 *version 1.8
 */
@ManagedBean(name="CProjetos")
public class CProjetos {
	private ArrayList<Projetos> lista = new ControleProjetos().consultarTodos();
	private String nome;
	private int id;
	private Part imagem;

	public Part getImagem() {
		return imagem;
	}
	public void setImagem(Part imagem) {
		this.imagem = imagem;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
		public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public ArrayList<Projetos> getLista(){
		return lista;
	}

	/**recebe e redireciona registro para o metodo de inserir 
	 * @return boolean
	 * @throws IOException
	 */
	public boolean adc() throws IOException {
		boolean result= false;
		Projetos pro= new Projetos();
		ControleProjetos co =new ControleProjetos();
		String pasta = "C:\\Users\\Bianca & Biatriz\\Desktop\\MeuProjeto\\WebContent\\imgUser\\";
		InputStream img = imagem.getInputStream();
		String nomee = imagem.getSubmittedFileName();
		Files.copy(img, new File(pasta,nomee).toPath(), StandardCopyOption.REPLACE_EXISTING);
		String diretImg = "imgUser/"+""+nomee;
		try {
			pro.setNome(nome);
			pro.setImagem(diretImg);
			co.inserir(pro);
			result=true;
		}catch(Exception e) {
			e.getMessage();
		}
		return result;
	}
	/**recebe e redireciona registro para o metodo de deletar
	 * @param id
	 * @return String
	 */
	public String deletar(int id) {
		new ControleProjetos().deletar(id);
		return "projetos";
	}
	/**recebe e redireciona registro para o metodo de consultar
	 * @param id
	 */
	public void carregarId(int id) {
		Projetos pro = new ControleProjetos().consultaUm(id);
		this.setId(pro.getId());
		this.setNome(pro.getNome());
	}
	/**recebe e redireciona registro para o metodo de editar
	 * @return String
	 */
	public String editar() {
		Projetos pro = new Projetos();
		ControleProjetos con = new ControleProjetos();
		pro.setId(id);
		pro.setNome(nome);
		if(con.atualizar(pro)) {
			return "projetos";	
		}else {
			return "atuProjetos";
		}
	}
}
